

#' Find the square root of power using synchrosqueezed signal
#'
#' Estimates the power within the given frequency. The 
#'  signal is decomposed using synchrosqueezed transform 
#'  from the SynchWave package. The latter is based on the
#'  Matlab implementation.
#'
#' @param x The input signal
#' @param fs The signal sampling rate
#' @param freqs A vector or list of vectors of the low and
#   high frequencies of for the bandpass filter.
#' @param tt The sample rate adjusted time of the signal 
#     observations
#'  
#'
#' @return  A double or list of doubles representing the
#'  signal's frequency in the given bandwidth(s)
#' 
#' @export 
synch_bandpass <- function(x, fs, freqs, tt = NULL){
  # TODO handle freuqeuncy for matrix or time series
  if(!is.list(freqs)){
    freqs <- list(freqs)
  }
  if(is.null(tt)){
    tt <- findxs(x, fs)
  }
  
  ss_bandpass <- function(fit, band){

    if((band[1] < min(fit$fs) || band[2] > max(fit$fs))){
      # cat("fs not found")
      bp <- 0

    } else {
      cwt <- SynchWave::synsq_filter_pass(fit$Tx, fit$fs, band[1], band[2])
      bp  <- SynchWave::cwt_iw(cwt$Txf, opt$type, opt)
    }
    sum(abs(bp))
  }
  
   # Options for SynchWave  
  nv <- 32
  opt <- list(type = "bump") 
  fit <- SynchWave::synsq_cwt_fw(tt, x, nv, opt)
  fqs <- lapply(freqs, function(y) y/fs)
  lapply(x, function(y) ss_bandpass(fit, y))
}


#' Find time values of signal based on sample rate
#' 
#' @param x A vector time series 
#' @param fs The sample rate
#'
#' @return 
#' @export
findxs <- function(x, fs){
   seq(0, (length(x)/fs), length.out = length(x))
}



#' Find the power within specified frequency bands
#'
#' Estimates the power within the given frequency 
#'  bands using Welch's method. The periogram is 
#'  then smoothed using wavelet thresholding. The 
#'  sum of power in bins within the frequency band
#'  is used as the estimate of that band's power.
#'
#' @param x The input signal
#' @param fs The signal sampling rate
#' @param freqs A vector or list of vectors of the
#'  bandwidth's whose power should be computed
#'
#' @return  A double or list of doubles representing the
#'  signal's frequency in the given bandwidths
#' 
#' @export
#' 
bandpower_spec <- function(x, fs, 
                          freqs, 
                          psd = NULL, 
                          smooth = TRUE){
  if(!is.list(freqs)){
    freqs <- list(freqs)
  }
  # TODO: handle class ts, frequency
  # covert x to frequency at frequency = fs
  if(fs < 256){
    seglen <- 256
  } else {
    seglen <- 1
  }
  # else seglen <- fs
  if(is.null(psd)){ 
    psd <- bspec::welchPSD(x, seglength = seglen)
  }
  if(smooth){
    wc <- waveslim::modwt(psd$power)
    # class(wc) <- "dwt"
    wc_th <- waveslim::sure.thresh(wc, max.level = 4, hard = FALSE)
    psd_smooth <- waveslim::imodwt(wc_th)
  }
  lapply(freqs, function(x) bandpower_one(psd_smooth, x)) 
  # list(res  = res, psd = psd)
}




#' Find the power within specified frequency bands
#'
#' Estimates the power within the given frequency 
#'  bands using Welch's method. The periogram is 
#'  then smoothed using wavelet thresholding. The 
#'  sum of power in bins within the frequency band
#'  is used as the estimate of that band's power.
#'
#' @param x The input signal
#' @param fs The signal sampling rate
#' @param freqs A vector or list of vectors of the
#'  bandwidth's whose power should be computed
#'
#' @return  A double or list of doubles representing the
#'  signal's frequency in the given bandwidths
#' 
#' @export
#' 
bandpower_spec2 <- function(x, fs, 
                          freqs, 
                          psd = NULL, 
                          smooth = TRUE){
  if(!is.list(freqs)) freqs <- list(freqs)
  if(is.null(psd)){ 
     psd <- spec.pgram(x, taper = 0.1, 
                          plot = FALSE,
                          fast = TRUE, 
                          kernel("modified.daniell", c(3,7)))
    tot <- sum(psd$spec)
  }
  lapply(freqs, function(x) bandpower_one(psd$spec, x)) 
  # list(res  = res, psd = psd)
}

lapply(frqs2l, function(x) bandpower_one(psd$spec, x)) 

bandpower_one <- function(psd, freqs){
  sum(psd[freqs[1]:freqs[2]])
}





#' Find the power within specified frequency bands
#'
#' Applies a Butterworth filter based on input 
#' frequency bands and returns power of signal in those
#' bands
#'
#' @param x The input signal
#' @param fs The signal sampling rate
#' @param freqs A vector or list of vectors of the
#'  bandwidth's whose power should be computed
#'
#' @return  A double or list of doubles representing the
#'  signal's frequency in the given bandwidths
#' 
#' @export
#' 
bandpower_fir <- function(x, fs, freqs){
  # Check format 
  if(!is.list(freqs)){
    freqs <- list(freqs)
  }
  
  lapply(freqs, function(y) signal::butter(n = 4, 
                                         W = hz2rad(y, fs), 
                                         type = "pass")) %>% 
  lapply(., function(y) signal::filter(y, x))  %>%
  lapply(., function(y) ts(y, frequency = fs))   %>%
  lapply(., 
          function(y) bspec::welchPSD(y, 
                         seglength = 1, # w.r.t input freq
                         # windowfun = tukeywindow, 
                         method = "mean", 
                         windowingPsdCorrection = TRUE)) %>%
  lapply(., function(y) sum(y$power))
}

  
# lapply(freqs, function(y) signal::butter(n = 4, 
#                                      W = hz2rad(y, fs), 
#                                      type = "pass"))  %>%
# lapply(., function(y) signal::filter(y, x)) %>%
# lapply(., function(y) ts(y, frequency = 1000)) %>%
# lapply(., 
#       function(y) bspec::welchPSD(y, 
#                      seglength = 1, # w.r.t input freq
#                      # windowfun = tukeywindow, 
#                      method = "mean", 
#                      windowingPsdCorrection = TRUE))  %>%
# lapply(., function(y) sum(abs(y$power)^2)) -> res

# convert hz to freqs
hz2rad <- function(freq, fs){
  2*freq/fs 
}

bandpower_fir <- function(x, fs, freqs){
  # Check format 
  if(!is.list(freqs)){
    freqs <- list(freqs)
  }
  
  bspec::welchPSD(y,  seglength = 1, # w.r.t input freq
                       # windowfun = tukeywindow, 
                       method = "mean", 
                       windowingPsdCorrection = TRUE)

}

bp_fft <- function(x){
  P = abs(2*fft(x)/100)^2; Fr = 0:(length(x)-1)/length(x)
  plot(Fr, P, type = 'l', xlab = 'frq')
}

fex1 <- function(){
# example from shumway p. 178
  x1 <- 2*cos(2*pi*1:1000*6/100) + 3*sin(2*pi*1:1000*6/100)
  x2 <- 4*cos(2*pi*1:1000*10/100) + 5*sin(2*pi*1:1000*10/100)
  x3 <- 6*cos(2*pi*1:1000*40/100) + 7*sin(2*pi*1:1000*40/100)
  x <- x1 + x2 + x3
  return(list(x1 = x1, x2 = x2, x3 = x3, x = x))
}

test_spec_power <- function(){
  set.seed(1)
  fs <- 1000
  xx <- seq(0, 5, length.out = 5000)
  x <- ts(sin(2*pi*xx*2.3) + 0.25*rnorm(length(xx)), frequency = fs)
  fxs <- fex1()  
  freqs <- list(
    # delta <- c(1,4),
    theta <- c(4,8),
    alpha1 <- c(8, 12),
    beta1 <- c(13, 20),
    beta2 <- c(20, 40),
    gamma <- c(40, 100))

  ret1 <- bandpower_spec(ts(fxs$x), 100, freqs)   
  ret2 <- synch_bandpass(fxs$x, 100, freqs)
  ret3 <- bandpower_fir(ts(fxs$x), 100, freqs)
}


test_fir_power <- function(){
  set.seed(1)
  fs <- 1000
  xx <- seq(0, 5, length.out = 5000)
  x <- ts(sin(2*pi*xx*2.3) + 0.25*rnorm(length(xx)), frequency = fs)


  freqs <- list(
    # delta <- c(1,4),
    theta <- c(4,8),
    alpha1 <- c(8, 12),
    beta1 <- c(13, 20),
    beta2 <- c(20, 40),
    gamma <- c(30, 100))
  bandpower_fir(x, fs, freqs)   
}

# fir1 <- test_fir_power()
# spec1 <- test_spec_power()


test_feature_spec <- function(){
  feature <- test_spec_power()
  res <- plyr::unrowname(data.frame((t(unlist(feature)))))
  names(res) <- unlist(lapply(1:length(res), function(x) paste0("bp", x)))
  # class(res) <- "bandpower_spec"
  res

}



