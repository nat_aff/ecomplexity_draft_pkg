#include <Rcpp.h>
#include <math.h>

using namespace Rcpp;


void polint(float xa[], float ya[], int n, float x, float *y, float *dy)
// Given arrays xa[1..n] and ya[1..n], and given a value x, this routine returns a value y, and
// an error estimate dy. If P(x) is the polynomial of degree N − 1 such that P(xai) = yai, i =
// 1,..., n, then the returned value y = P(x).
{ 
  int i, m, ns=1;
  float den,dif,dift,ho,hp,w;
  
  float *c, *d; 

  dif=fabs(x-xa[1]);
  c = std::vector<double>(1,n);
  d = std::vector<double>(1,n);
  for (i=1;i<=n;i++) { 
    //Here we find the index ns of the closest table entry,
    if ( (dift=fabs(x-xa[i])) < dif) {
    ns=i;
    dif=dift;
    }
    c[i]=ya[i];  // and initialize the tableau of c’s and d’s.
    d[i]=ya[i];
  } // end for
  *y=ya[ns--];    // This is the initial approximation to y.
  for (m=1;m<n;m++) {
   //For each column of the tableau,
    for (i=1;i<=n-m;i++) { 
      // we loop over the current c’s and d’s and update them.
      ho=xa[i]-x; 
      hp=xa[i+m]-x;
      w=c[i+1]-d[i];
      if ( (den=ho-hp) == 0.0) std::cout <<  "Error in routine polint" << std::endl;
    // This error can occur only if two input xa’s are (to within roundoff) identical.
    // den=w/den;
    d[i]=hp*den;  //Here the c’s and d’s are updated.
    c[i]=ho*den;
    }
  *y += (*dy=(2*ns < (n-m) ? c[ns+1] : d[ns--]));
  // After each column in the tableau is completed, we decid  
  }
  free_vector(d,1,n);
  free_vector(c,1,n);
}

