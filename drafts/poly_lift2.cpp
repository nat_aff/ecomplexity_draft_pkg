#include <Rcpp.h>
#include <iostream>
using namespace Rcpp;

#define EPSILON 1.0e-20;    /* A small number */
#define NEVILLE_SIZE "Vectors must be the same length";
#define DENOM_ZERO "Index points are not unique";




// [[Rcpp::export]]
double predict(NumericVector ys, NumericVector filter)
{   
    // check sizes are equal
    int n, k;
    double sum = 0;

    n = ys.size();   
    for(k = 0; k < n; k++){
        sum += filter[k] * ys[k];
    }
    return sum; 
}


// [[Rcpp::export]]
double neville(Numeric Vector x, Numeric Vector y, double t) {
    // stopifnot(is.numeric(x), is.numeric(y))
    // if (!is.numeric(xs))
    //     stop("Argument 'xs' must be empty or a numeric vector.")
        
    // x <- c(x); y <- c(y)
    int n = x.size();
    NumericVector yy(n);

    if(n != y.size()){
        // std::cout << "x.size() != y.size()" << std::endl;
        throw Rcpp::exception("neville", NEVILLE_SIZE)
    }

    // for (int k = 1; k < n; k++) {
    //     y[1:(n-k)] = ((xs - x[(k+1):n]) * y[1:(n-k)] +
    //                    (x[1:(n-k)] - xs) * y[2:(n-k+1)]) /
    //                    (x[1:(n-k)] - x[(k+1):n])
    // }
    for(int i = 0; i < n; i++){
           double den = x[i] - x[j];
            if (den < EPSILON || den > -EPSILON ) {
                throw Rcpp::exception ("Neville", DENOM_ZERO);
            }
            y[j] = y[j+1] + (y[j+1] - y[j]) * (t - x[i]) / den;
        }
    }

    // ys <- y[1]
    return y[0];
}



















