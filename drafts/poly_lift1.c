#include <Rcpp.h>
#include <iostream>
using namespace Rcpp;

#define EPSILON 1.0e-20;    /* A small number */
#define NEVILLE_SIZE "Vectors must be the same length";
#define DENOM_ZERO "Index points are not unique";
// [[Rcpp::export]]
double neville(Numeric Vector x, Numeric Vector y, double t) {
    // stopifnot(is.numeric(x), is.numeric(y))
    // if (!is.numeric(xs))
    //     stop("Argument 'xs' must be empty or a numeric vector.")
        
    // x <- c(x); y <- c(y)
    int n = x.size();
    NumericVector yy(n);

    if(n != y.size()){
        // std::cout << "x.size() != y.size()" << std::endl;
        throw Rcpp::exception("neville", NEVILLE_SIZE)
    }

    // for (int k = 1; k < n; k++) {
    //     y[1:(n-k)] = ((xs - x[(k+1):n]) * y[1:(n-k)] +
    //                    (x[1:(n-k)] - xs) * y[2:(n-k+1)]) /
    //                    (x[1:(n-k)] - x[(k+1):n])
    // }
    for(int i = 0; i < n; i++){
           double den = x[i] - x[j];
            if (den < EPSILON || den > -EPSILON ) {
                throw Rcpp::exception ("Neville", DENOM_ZERO);
            }
            y[j] = y[j+1] + (y[j+1] - y[j]) * (t - x[i]) / den;
        }
    }

    // ys <- y[1]
    return y[0];
}



// Neville ( const Flt *x, const Flt *f, const int n, const Flt xx )
// {
//     register int i,j;
//     Vector vy;
//     Flt y;

//     vy = vector( 0, (long)(n-1) );   /* allocate a temporary vector */
//     for ( i=0; i<n; i++ ) {
//         vy[i] = f[i];
//         for ( j=i-1; j>=0; j--) {
//             Flt den = x[i] - x[j];
//             if ( zero(den) ) {
//                 Error ("Neville", DENOM_NEAR_ZERO, RETURN);
//             }
//             vy[j] = vy[j+1] + (vy[j+1] - vy[j]) * (xx - x[i]) / den;
//         }
//     }
//     y = vy[0];

//     /* Free memory */
//     free_vector( vy, 0, (long)(n-1) );

//     return y;
// }


//  # ps <- double(15)
// order <- 1
// filters <- cmats[[order]]
// N <- order + 1

// for(k in 1:(N-2)){
//     # use filter k
//     cat("k", k, "\n")
//     ps[k] <- predict(tys[k:(k+3)], filters[k,])
// }
// # todo check end point (N-1)
// for(k in N:(length(ps)-(N-2))){
//     cat("k", k, "\n")
//     ps[k] <- predict(tys[k:(k+3)], filters[(N-1),])
// }
// j <- 0
// for(k in (length(ps)-(N-3)):length(ps)){
//     cat("k, :", k, "\n")
//     # cat("j:", j, "\n")
//     ps[k]  <- predict(tys[k:(k+3)], filters[N+j,])
//     j <- j + 1
// }


















