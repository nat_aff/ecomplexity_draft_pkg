#include <Rcpp.h>
#include <iostream>
using namespace Rcpp;
 

// [[Rcpp::export]]
NumericVector pdistC(double x, NumericVector ys) {
  int n = ys.size();
  NumericVector out(n);

  for(int i = 0; i < n; ++i) {
    out[i] = sqrt(pow(ys[i] - x, 2.0));
  }
  std::cout << "pdistC" << std::endl;
  return out;
}
