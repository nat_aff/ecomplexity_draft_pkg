#include <Rcpp.h>
#include <math.h>
using namespace Rcpp;

// xs -- sorted vector of x coordinates
// ix is the value to interpolate
// [[Rcpp::export]]
double poly_interp(NumericVector xs, NumericVector ys, int x, int n) {
  // int n = ys.size(); 
  int ns = 1;
  double yout, den, diff, difft, ho, hp, w;
  NumericVector c(n);
  NumericVector d(n);

  diff = fabs(x - xs[1]);

  for(int i = 2; i <= n; i++){
      difft = fabs(x-xs[i]);
    if( difft < diff ){
      ns = i;
      diff = difft;
    }
    c[i] = ys[i];
    d[i] = ys[i];
  } 
  
  yout = ys[ns--];
  
  for (int m = 1; m < n; m++ ){
    for (int i = 1; i <= n; i++ ){
      ho = xs[i] - x;
      hp = xs[i+m]-x;
      w = c[i+1] - d[i];
      den = ho-hp; 
      den = w/den;
      d[i] = hp*den;
      c[i] = ho*den;
    } // end for2
    if( 2*ns < (n-m)){
      yout += c[ns + 1];
    } else {
      yout += d[ns--];
    }
  } // end for1
  
  return yout;
} 