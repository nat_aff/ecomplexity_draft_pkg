
#' Find Nterm Complexity of a Time Series
#'
#' Find the nterm complexity of a function
#'
#' @param ys A vector, time series (or matrix ... ?) 
#' @param verbose Print debugging information
#' @param slope_only Return only the slope
#'
#' @return A list or the slope coefficient of the nterm approximation
nterm_approx_test <- function(ys, verbose = FALSE, slope_only = FALSE, ...){
  trim = FALSE
  if(verbose) cat("verbose ...")
  
  ys <- normalize(ys)
  # param :
  # number of bins
  start <- 30
  nbins <- 100
  ylen <- length(ys)
  wc <- waveslim::modwt(ys)
  # remove smooth coefficients   
  # xsort <- sort(abs(x[1:(length(x)- ylen)])) 
  thresholds <- seq(.05, .5, by = 0.05)
  wc_sort <- sort(abs(unlist(wc))) 
  errors <- fits <- vector("list", length(thresholds))
  for(k in seq_along(thresholds)){
    threshold <- thresholds[k]
    # if(verbose) sprintf("theta = %2f", theta)
    wc_thresh <- manual_thresh2(wc, value = threshold, 
                                max.level = length(wc))
    # if() checkTerms(wcThresh)
    fits[[k]] <- waveslim::imodwt(wc_thresh)
    errors[[k]] <- sum((ys - fits[[k]])^2)/length(ys)
  }
  #Note : magic number 3
  
  # if(trim){
  #   trimmed_errors <- errors[3:18]
  #   coefficients <- fit_terms(trimmed_errors)
  # } else {
  coefficients <- fit_terms(errors)
  # }
  # coefficients <- fit_terms(errors)
  if(slope_only){ 
    res <- coefficients[2]
  } else { 
    res <- list(errors = errors, 
              thetas =  thetas, 
              fits = fits, 
              coefficients = coefficients)
  }
  res 
}


# Used by nterm_approx
fit_terms <- function(y){
  x <- 1:length(y)
  fit <- lm(unlist(y) ~ x)
  fit$coefficients
} 


# Used in previous debugging of nterm_approx
checkTerms <- function(wc){
  x <- unlist(wc)
  nonzero <- sum(abs(unlist(wc)) > 0)
  cat("nonzero: ", nonzero, "\n")
}


#' Normalize vector to range [0,1]
#'
#'@param x The vectorizable data to be normalized
#'@return The normalized sequence
normalize <- function(x){
    abs(x)/max(abs(x)) 
}


# Adapted from waveslim::manual_thresh
manual_thresh2 <- function (wc, max.level = 4, value, hard = TRUE) {
    wc.shrink <- wc
    if (hard) {
        for (i in names(wc)[1:max.level]) {
            wci <- wc[[i]]
            # unithresh <- factor * value
            unithresh <- value
            wc.shrink[[i]] <- wci * (abs(wci) > unithresh)
        }
    } else {
        for (i in names(wc)[1:max.level]) {
            wci <- wc[[i]]
            unithresh <- factor * value
            wc.shrink[[i]] <- sign(wci) * (abs(wci) - unithresh) * 
                (abs(wci) > unithresh)
        }
    }
    wc.shrink
}



# Used to plot multiple versions of fit
# Needs update
plotNtermFits <- function(ys, fits, pal){

  plot(ys[1:100], type = "l", col = "black", lwd = 1.7, lty = 1.7, xlab = "", ylab = "")
  
  for (k in 1:length(fits)) {
    par(new = TRUE)
    plot(fits[[k]][1:100], type = "l", lwd = 1.6, col = pal[k], 
              xaxt = "none", yaxt = "none", ylab = "", xlab ="")
  }
  par(new = TRUE)
   plot(ys[1:100], type = "l", col = "black", 
                lwd = 1.8, lty = 2, xlab = "", ylab = "")
 
}


test_one_ts <- function(){
  f <- arima.sim(n = 200, list(ar = c(0.8897, -0.4858), 
                ma = c(-0.2279, 0.2488)), sd = sqrt(0.1796))
  res <- nterm_approx_test(f, slope_only = TRUE)
  is.numeric(res)
}




grp1 <- replicate(10, arima.sim(n = 200, list(ar = c(0.8897, -0.4858), 
  ma = c(-0.2279, 0.2488)), sd = sqrt(0.1796)))

grp2 <- replicate(10, arima.sim(n = 200, list(ar = c(-0.71, 0.18), ma = c(0.92, 0.14)), 
  sd = sqrt(0.291)))

g1 <- apply(grp1, 2, function(x) nterm_approx_test(x, slope_only=TRUE))
g2 <- apply(grp2, 2, function(x) nterm_approx_test(x, slope_only=TRUE))




# Check if running
run_test <- function(n){
  groups <- test_functions()
  fs <- lapply(groups$group1, generate) 
  fxs <- lapply(fs, function(x) x(n))
  res <- lapply(fxs, function(x) nterm_approx_test(x, 
                      slope_only = FALSE, 
                      wf = "la8", 
                      n.levels = 4, 
                      boundary = "periodic") ) 
 res2 <- lapply(res, function(x) x$coefficients[2] )
 errs <- lapply(res, function(x) x$errors)
 return(list(funcs = groups, errors = errs, res = res, coeffs = res2))
}



#------------------------------------
# Plot linear fit of all functions
plot_errs <- function(y, fname){
  plot(log(y), 
    pch  = 16, cex = 0.8, 
    col  = "gray40", 
    ylab = "log(epsilons)", 
    xlab = "n", 
    main = fname)
  
  x <- 1:length(y)
  fit <- lm(log(y) ~ x) 
  abline(fit, col = "darkblue", lwd = "2")
}


# old
plot_all_fits <- function(res){
  # plotNtermFits(res$funcs[[1]][[3]], res$
  errs <- lapply(res$errors, unlist)

  par(mfrow = c(3,2))

  for(k in seq_along(errs)){
    plot_errs(errs[[k]], names(errs[k]))
  }


}

  