#include <Rcpp.h>
#include <math.h>
using namespace Rcpp;

// xs -- sorted vector of x coordinates
// ix is the value to interpolate
// [[Rcpp::export]]
double poly_interp( int t, int n, NumericVector xs, NumericVector ys) {
  // int len = ys.size(); 
  int ns = 1;
  double yout, den, diff, difft, ho, hp, w;
  NumericVector c(n);
  NumericVector d(n);

  diff = fabs(t - xs[1]);

  for(int i = 1; i <= n; i++){
      difft = fabs(t - xs[i]);
    if( difft < diff ){
      ns = i;
      diff = difft;
    }
    c[i] = ys[i];
    d[i] = ys[i];
  } 
 
  yout = ys[ns--];

  for (int m = 1; m < n; m++ ){
    for (int i = 1; i <= n; i++ ){
      ho = xs[i] - t;
      hp = xs[i+m] - t;
      w = c[i+1] - d[i];
      den = ho - hp; 
      den = w/den;
      d[i] = hp*den;
      c[i] = ho*den;
    } // end for2
    if( 2*ns < (n-m)){
      yout += c[ns + 1];  
    } else {
      yout += d[ns--];
    }
  } // end for1
  
  return yout;
} 




// [[Rcpp::export]]
double poly_interp2(int t, int n, NumericVector xs, NumericVector ys ) {
  int i, j, k;  
  int len = xs.size();
  NumericVector yout(len);

  for(k = 1; k <= len; k++){
    yout[k] = ys[k];
  }

  for (j = 1; j < n; j++ ){
    for (i = n - 1; i >= j; i-- ) {
      yout[i] = ( (t-xs[i-j])*yout[i] - (t-xs[i])*yout[i-1] ) / ( xs[i] - xs[i-j] );
    }
  }

 double total = yout[n-1];
 return total;  

}



/**
     @param x the x-coordinate of the interpolated point
     @param N the number of polynomial points.
     @param c[] an array for returning the coefficients

     */
// [[Rcpp::export]]
NumericVector lagrange( double x, int N )
  {
    double num, denom;
    NumericVector c(N);

    for (int i = 0; i < N; i++) {
      num = 1;
      denom = 1;
      for (int k = 0; k < N; k++) {
  if (i != k) {
    num = num * (x - k);
    denom = denom * (i - k);
  }
      } // for k
      c[i] = num / denom;
    } // for i
    return c;
  } // lagrange


// NumericMatrix fillTable( const int N )
//   {
//     NumericMatrix table(4,4);
//     double x;
//     double n = N;
//     int i = 0;

//     for (x = 0.5; x < n; x = x + 1.0) {
//       lagrange( x, N);
//       i++;
//     }
//   } // fillTable

// NumericVector interp(NumericMatrix)




















