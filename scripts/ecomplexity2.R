library(splines)

#' Normalize vector to range [0,1]
#'
#'@param x The vectorizable data to be normalized
#'@return The normalized sequence
normalize <- function(x){
    abs(x)/max(abs(x)) 
}


ecomplexity2 <- function(ys, slope_only = TRUE, max_downsample = 6, max_degree = 5){
  ys <- normalize(ys)
  epsilons <- double(max_downsample-1)
  ds <- 2:max_downsample
  for(k in ds){
    epsilons[k-1] <- epsilon_err(ys, k, max_degree)
  }

  fit <- lm(log(epsilons) ~ log(1/ds))   
  epsilons
  if(slope_only){
    res <- fit$coefficients[2]
  } else {
    res <- list(fit = fit, epsilons = epsilons, 
                max_downsample = max_downsample, max_degree = max_degree)
    class(res) <- "complexity_feature"
  }
  res
}



#' Function returns result for a single downsample 
#' level
#' 
#' Function returns result for a single downsample 
#' level
#'
#'@param ys : A vector or time series 
#'@param sample_num : The amount the series is downsampled
#'@param max_degree : The maximum degree spline polynomial to fit
epsilon_err <- function(ys, sample_num, max_degree){
  # cat("epsilon err: sample_num: ", sample_num, "\n")
  xx <- 1:length(ys)
  df <- data.frame(x = xx,y = ys); 
  # get list of valid indices at given downsample rate
  indices  <- downsample_perm(length(ys), sample_num);
  # errors for each permutation
  epsilons <- double(length(indices))
  # cat("sample_num:", sample_num, "\n")  
  for (k in 1:sample_num) {
    cur_knots = indices[[k]]
    temp      = 1:length(xx)
    hold_out  = temp[-cur_knots];

    # Mean absolute error for each degree spline
    maes <- matrix(0, nrow = max_degree, 
                      ncol = length(hold_out) )
    for (d in 1:max_degree){
        # create model based on cur_knots
        basis    <- bs(xx, knots = cur_knots, degree = d)
        mod      <- lm(y ~ basis, data = df );
        maes[d,] <- abs(ys[hold_out] - predict(mod)[hold_out])
    }
      epsilons[k]  <- min(apply(maes, 1, sum)) 
      # cat("length maes :", length(min_errs), '\n')
  }
  # cat("length eps :", length(epsilons), '\n')
  return(mean(epsilons))
}




#' Function returns all possible downsample patterns
#' where for a sequence of length n downsample at rate ds
#'
#' Function returns all possible downsample patterns
#' where for a sequence of length n downsample at rate ds
#'
#'@param n 
#'@param ds
downsample_perm <- function(n,ds){
  xx <- 1:n
  ind  <- vector("list", ds)    
  for (k in 1:ds){
    tseq   <- xx[(xx -(k-1))%% ds == 0] 
      ind[[k]] <- tseq
  }
  ind
}




